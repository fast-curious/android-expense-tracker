# The `app` module

This module is module that integrates all the features into an app. 
Nothing should depend on it, but it should be dependent on all the features.