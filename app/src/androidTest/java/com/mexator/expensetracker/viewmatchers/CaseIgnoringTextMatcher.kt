package com.mexator.expensetracker.viewmatchers

import android.view.View
import android.widget.TextView
import androidx.test.espresso.matcher.BoundedMatcher
import org.hamcrest.Description
import org.hamcrest.Matcher
import org.hamcrest.text.IsEqualIgnoringCase

fun withTextIgnoringCase(string: String): Matcher<View> {
    return object : BoundedMatcher<View, TextView>(TextView::class.java) {
        val matcher = IsEqualIgnoringCase(string)

        override fun describeTo(description: Description) {
            description.appendText("with text (ignoring case): $string")
        }

        public override fun matchesSafely(textView: TextView): Boolean {
            return matcher.matchesSafely(textView.text.toString())
        }
    }
}
