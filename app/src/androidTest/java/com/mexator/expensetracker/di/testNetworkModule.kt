package com.mexator.expensetracker.di

import android.util.Log
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import org.koin.core.qualifier.named
import org.koin.dsl.module

val testNetworkModule = module {
    val logging = HttpLoggingInterceptor {
        Log.i("OkHttp", it)
    }.setLevel(HttpLoggingInterceptor.Level.BODY)

    single(named("authorized")) {
        OkHttpClient.Builder().addInterceptor(logging).build()
    }

    single(named("unauthorized")) {
        OkHttpClient.Builder().addInterceptor(logging).build()
    }
}
