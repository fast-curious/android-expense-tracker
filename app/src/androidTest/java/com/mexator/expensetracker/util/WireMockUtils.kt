package com.mexator.expensetracker.util

import androidx.test.platform.app.InstrumentationRegistry
import com.github.tomakehurst.wiremock.client.MappingBuilder
import com.github.tomakehurst.wiremock.client.ResponseDefinitionBuilder
import com.github.tomakehurst.wiremock.client.WireMock.*
import kotlinx.serialization.json.Json
import kotlinx.serialization.json.encodeToJsonElement
import java.io.BufferedReader
import java.io.InputStreamReader

fun fileToString(path: String): String {
    return BufferedReader(
        InputStreamReader(
            InstrumentationRegistry.getInstrumentation().context.assets.open(path),
            Charsets.UTF_8
        )
    ).readText()
}

object WireMockUtils {
    fun okFromAssets(
        path: String,
        vararg formatArgs: Any? = emptyArray()
    ): ResponseDefinitionBuilder {
        var stringFile = fileToString(path)
        if (formatArgs.isNotEmpty()) stringFile = stringFile.format(*formatArgs)
        return ok(stringFile)
    }

    inline fun <reified T : Any> okWithValue(value: T): ResponseDefinitionBuilder {
        val response = Json.encodeToJsonElement(value)
        return ok(response.toString())
    }

    inline fun withStub(request: MappingBuilder, crossinline block: () -> Unit) {
        val requestStub = stubFor(request)
        block()
        removeStub(requestStub)
    }
}
