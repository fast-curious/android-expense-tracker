package com.mexator.expensetracker.feature.transactions

import androidx.fragment.app.testing.FragmentScenario.Companion.launchInContainer
import androidx.recyclerview.widget.RecyclerView.ViewHolder
import androidx.test.espresso.Espresso.onView
import androidx.test.espresso.action.ViewActions.click
import androidx.test.espresso.action.ViewActions.typeText
import androidx.test.espresso.assertion.ViewAssertions.matches
import androidx.test.espresso.contrib.RecyclerViewActions.scrollTo
import androidx.test.espresso.matcher.RootMatchers
import androidx.test.espresso.matcher.ViewMatchers.hasDescendant
import androidx.test.espresso.matcher.ViewMatchers.isClickable
import androidx.test.espresso.matcher.ViewMatchers.isDescendantOfA
import androidx.test.espresso.matcher.ViewMatchers.isDisplayed
import androidx.test.espresso.matcher.ViewMatchers.withId
import androidx.test.espresso.matcher.ViewMatchers.withText
import androidx.test.ext.junit.runners.AndroidJUnit4
import com.github.tomakehurst.wiremock.client.WireMock.equalTo
import com.github.tomakehurst.wiremock.client.WireMock.get
import com.github.tomakehurst.wiremock.client.WireMock.getRequestedFor
import com.github.tomakehurst.wiremock.client.WireMock.post
import com.github.tomakehurst.wiremock.client.WireMock.postRequestedFor
import com.github.tomakehurst.wiremock.client.WireMock.serverError
import com.github.tomakehurst.wiremock.client.WireMock.stubFor
import com.github.tomakehurst.wiremock.client.WireMock.urlPathMatching
import com.github.tomakehurst.wiremock.client.WireMock.verify
import com.github.tomakehurst.wiremock.junit.WireMockRule
import com.mexator.expensetracker.di.testNetworkModule
import com.mexator.expensetracker.domain.common.testUrl
import com.mexator.expensetracker.domain.transactions.di.domainTransactionsModule
import com.mexator.expensetracker.domain.transactions.model.RUB
import com.mexator.expensetracker.domain.transactions.model.Transaction
import com.mexator.expensetracker.domain.transactions.repository.TransactionsRepository.Companion.PAGE_SIZE
import com.mexator.expensetracker.feature.transactions.di.featureTransactionsModule
import com.mexator.expensetracker.feature.transactions.ui.TransactionsFragment
import com.mexator.expensetracker.util.WireMockUtils.okFromAssets
import com.mexator.expensetracker.util.WireMockUtils.okWithValue
import com.mexator.expensetracker.util.WireMockUtils.withStub
import com.mexator.expensetracker.viewmatchers.hasEditTextHintText
import com.mexator.expensetracker.viewmatchers.withTextIgnoringCase
import org.hamcrest.core.AllOf.allOf
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.junit.runner.RunWith
import org.koin.core.context.startKoin
import org.koin.core.context.stopKoin
import java.util.*

/**
 * To test:
 * - Screen layout [testLayout]
 *     - Backend returns only one transaction
 *     - Open screen
 *     - Check that screen contains:
 *          + Create transaction fields
 *            - Header
 *            - name field
 *            - amount field
 *            - "create" button
 *            - currency field
 *          + List
 *            - name of transaction
 *            - amount (int sum + currency)
 *
 * - Creation of transaction [testCreateTransaction]
 *     - Open screen
 *     - Enter values in fields
 *     - Press submit
 *     - Check request sent
 *     - Check item added to the list
 *
 * - Pagination [testPagination]
 *     - Open screen
 *     - Fail load of the very first page
 *     - Check error displayed
 *     - Tap retry
 *     - Check first page loaded
 *     - Scroll to the bottom
 *     - Check second page requested
 *     - Check second page displayed
 *     - Scroll to the bottom
 *     - Fail load of third page
 *     - Check page loading error displayed
 *     - Tap retry
 *     - Check third page displayed
 *     - Scroll to the bottom
 *     - Check fourth page requested
 *     - Check fourth page displayed
 */
@RunWith(AndroidJUnit4::class)
class FeatureTransactionsTest {

    @get:Rule
    val rule = WireMockRule(5005)

    @Before
    fun setUp() {
        stopKoin()
        startKoin {
            modules(
                testNetworkModule, domainTransactionsModule(testUrl), featureTransactionsModule
            )
        }
        stubFor(
            get(urlPathMatching("/currencies"))
                .willReturn(serverError())
        )
    }

    @Test
    fun testLayout() {
        val transactionsList = onView(withId(R.id.transactions_list))
        // - Backend returns only one transaction
        stubFor(
            get(urlPathMatching("/transactions"))
                .willReturn(okFromAssets("transactions/response/noTransactions.json"))
        )
        stubFor(
            get(urlPathMatching("/transactions"))
                .withQueryParam("offset", equalTo("0"))
                .willReturn(okFromAssets("transactions/response/singleTransaction.json"))
        )

        // - Open screen
        launchInContainer(TransactionsFragment::class.java, themeResId = R.style.AppTheme)

        // - Check that screen contains:
        // + Create transaction fields
        onView(withText("Add a transaction")).check(matches(isDisplayed()))
        onView(hasEditTextHintText("Transaction name")).check(matches(isDisplayed()))
        onView(hasEditTextHintText("Amount")).check(matches(isDisplayed()))
        onView(withTextIgnoringCase("Add to the list")).check(
            matches(
                allOf(
                    isDisplayed(),
                    isClickable()
                )
            )
        )
        onView(hasEditTextHintText("Currency")).check(matches(isDisplayed()))
        // + List
        Thread.sleep(400)
        transactionsList.check(matches(isDisplayed()))
        onView(withText("The NAME")).check(matches(isDisplayed()))
        onView(withText("10 rub")).check(matches(isDisplayed()))
    }

    @Test
    fun testCreateTransaction() {
        val name = "CREATE"
        val amount = "100"

        stubFor(
            get(urlPathMatching("/transactions"))
                .willReturn(okFromAssets("transactions/response/noTransactions.json"))
        )
        stubFor(
            post(urlPathMatching("/transactions"))
                .willReturn(okFromAssets("transactions/response/createTransaction.json"))
        )

        // - Open screen
        launchInContainer(TransactionsFragment::class.java, themeResId = R.style.AppTheme)

        // - Enter values in fields
        onView(hasEditTextHintText("Transaction name")).perform(
            click(),
            typeText(name)
        )
        onView(hasEditTextHintText("Amount")).perform(
            click(),
            typeText(amount)
        )
        onView(hasEditTextHintText("Currency")).perform(
            click(),
        )
        onView(withText("rub"))
            .inRoot(RootMatchers.isPlatformPopup())
            .perform(click())
        // - Press submit
        onView(withTextIgnoringCase("Add to the list")).perform(click())
        // - Check request sent
        verify(postRequestedFor(urlPathMatching("/transactions")))
        // - Check item added to the list
        onView(
            allOf(
                withText(name),
                isDescendantOfA(withId(R.id.transactions_list))
            )
        ).check(matches(isDisplayed()))
    }

    @Test
    fun testPagination() {
        val transactionsList = onView(withId(R.id.transactions_list))
        // - Open screen
        // - Fail load of the very first page
        // - Check error displayed
        withStub(
            get(urlPathMatching("/transactions"))
                .willReturn(serverError())
        ) {
            launchInContainer(TransactionsFragment::class.java, themeResId = R.style.AppTheme)
            Thread.sleep(400)
            onView(withText("Error loading transactions: \n HTTP 500 Server Error"))
                .check(matches(isDisplayed()))
        }

        stubFor(
            get(urlPathMatching("/transactions"))
                .willReturn(okFromAssets("transactions/response/noTransactions.json"))
        )
        repeat(4) {
            stubFor(
                get(urlPathMatching("/transactions"))
                    .withQueryParam("offset", equalTo((PAGE_SIZE * it).toString()))
                    .willReturn(okWithValue(generatePage(it)))
            )
        }
        // - Tap retry
        onView(allOf(withTextIgnoringCase("Retry"))).perform(click())
        Thread.sleep(400)
        // - Check first page loaded
        // - Scroll to the bottom
        transactionsList.perform(
            scrollTo<ViewHolder>(
                hasDescendant(withText("Page 1 transaction 13"))
            )
        )
        // - Check second page requested
        // - Check second page displayed
        Thread.sleep(400)
        verify(
            getRequestedFor(urlPathMatching("/transactions"))
                .withQueryParam("offset", equalTo("15"))
        )
        transactionsList.perform(
            scrollTo<ViewHolder>(
                hasDescendant(withText("Page 2 transaction 0"))
            )
        )
        // - Scroll to the bottom
        // - Fail load of third page
        // - Check page loading error displayed
        withStub(
            get(urlPathMatching("/transactions"))
                .withQueryParam("offset", equalTo("30"))
                .willReturn(serverError())
        ) {
            transactionsList.perform(
                scrollTo<ViewHolder>(
                    hasDescendant(withText("Page 2 transaction 13"))
                )
            )
            transactionsList.perform(
                scrollTo<ViewHolder>(
                    hasDescendant(withText("Error receiving next page"))
                )
            )
        }
        // - Tap retry
        onView(allOf(withTextIgnoringCase("Retry"))).perform(click())
        // - Check third page displayed
        // - Scroll to the bottom
        transactionsList.perform(
            scrollTo<ViewHolder>(
                hasDescendant(withText("Page 3 transaction 12"))
            )
        )
        // - Check fourth page requested
        // - Check fourth page displayed
        Thread.sleep(400)
        verify(
            getRequestedFor(urlPathMatching("/transactions"))
                .withQueryParam("offset", equalTo("45"))
        )
        transactionsList.perform(
            scrollTo<ViewHolder>(
                hasDescendant(withText("Page 4 transaction 0"))
            )
        )
    }

    private fun generatePage(page: Int, quantity: Int = PAGE_SIZE): List<Transaction> {
        return (0..quantity).map { offset ->
            val id = page * PAGE_SIZE + offset
            Transaction(
                id = id,
                currency = RUB,
                amount = id.toDouble(),
                name = "Page ${page + 1} transaction $offset",
                comment = "",
                creationDate = Date(),
                tag = null,
                baseAmount = null
            )
        }
    }
}
