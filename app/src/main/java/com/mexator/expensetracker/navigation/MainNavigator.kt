package com.mexator.expensetracker.navigation

import android.content.Context

interface MainNavigator {
    fun navigateToTransactions(context: Context)
    fun navigateToProfile(context: Context)
}
