package com.mexator.expensetracker

import android.app.Application
import com.mexator.expensetracker.di.localStorageModule
import com.mexator.expensetracker.di.navigationModule
import com.mexator.expensetracker.di.networkModule
import com.mexator.expensetracker.domain.auth.di.domainAuthModule
import com.mexator.expensetracker.domain.common.realUrl
import com.mexator.expensetracker.domain.transactions.di.domainTransactionsModule
import com.mexator.expensetracker.feature.auth.di.featureAuthModule
import com.mexator.expensetracker.feature.splash.di.featureSplashModule
import com.mexator.expensetracker.feature.transactions.di.featureTransactionsModule
import org.koin.android.ext.koin.androidContext
import org.koin.core.context.startKoin

class ExpenseTrackerApp : Application() {
    override fun onCreate() {
        super.onCreate()
        startKoin {
            androidContext(this@ExpenseTrackerApp)
            modules(
                networkModule, navigationModule, localStorageModule,
                domainTransactionsModule(realUrl), featureTransactionsModule,
                domainAuthModule, featureAuthModule,
                featureSplashModule
            )
        }
    }
}
