package com.mexator.expensetracker

import android.os.Bundle
import android.view.MenuItem
import androidx.appcompat.app.AppCompatActivity
import androidx.fragment.app.commit
import com.mexator.expensetracker.databinding.ActivityMainBinding
import com.mexator.expensetracker.feature.transactions.ui.TransactionsFragment
import com.mexator.expensetracker.navigation.MainNavigator
import org.koin.android.ext.android.inject

class MainActivity : AppCompatActivity() {

    private lateinit var binding: ActivityMainBinding
    private val mainNavigator: MainNavigator by inject()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityMainBinding.inflate(layoutInflater)
        setContentView(binding.root)

        binding.mainNavigationBar.setOnItemSelectedListener(this::handleNavigation)

        supportFragmentManager.commit {
            replace(R.id.container, TransactionsFragment())
        }
    }

    private fun handleNavigation(item: MenuItem): Boolean {
        when (item.itemId) {
            R.id.screen_transactions -> mainNavigator.navigateToTransactions(this)
            R.id.screen_profile -> mainNavigator.navigateToProfile(this)
        }
        return true
    }
}
