package com.mexator.expensetracker.di

import android.content.Context
import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import androidx.fragment.app.FragmentManager
import androidx.fragment.app.commit
import androidx.fragment.app.replace
import com.mexator.expensetracker.MainActivity
import com.mexator.expensetracker.R.id.container
import com.mexator.expensetracker.feature.auth.AuthActivity
import com.mexator.expensetracker.feature.auth.R
import com.mexator.expensetracker.feature.auth.login.navigation.LoginNavigator
import com.mexator.expensetracker.feature.auth.login.ui.LoginFragment
import com.mexator.expensetracker.feature.auth.register.navigation.RegisterNavigator
import com.mexator.expensetracker.feature.auth.register.ui.RegisterFragment
import com.mexator.expensetracker.feature.profile.navigation.ProfileNavigator
import com.mexator.expensetracker.feature.profile.ui.ProfileFragment
import com.mexator.expensetracker.feature.splash.navigation.SplashNavigator
import com.mexator.expensetracker.feature.transactions.ui.TransactionsFragment
import com.mexator.expensetracker.navigation.MainNavigator
import org.koin.dsl.binds
import org.koin.dsl.module

val navigationModule = module {
    single {
        object : LoginNavigator, RegisterNavigator {
            override fun navigateToTransactions(context: Context) {
                val intent = Intent(context, MainActivity::class.java)
                intent.flags = Intent.FLAG_ACTIVITY_NEW_TASK or Intent.FLAG_ACTIVITY_CLEAR_TASK
                context.startActivity(intent)
            }

            override fun navigateToLogin(context: Context) {
                (context as AppCompatActivity).supportFragmentManager
                    .apply(FragmentManager::popBackStack)
                    .commit {
                        replace<LoginFragment>(R.id.content)
                        addToBackStack(null)
                    }
            }

            override fun navigateToRegister(context: Context) {
                (context as AppCompatActivity).supportFragmentManager
                    .apply(FragmentManager::popBackStack)
                    .commit {
                        replace<RegisterFragment>(R.id.content)
                        addToBackStack(null)
                    }
            }
        }
    } binds arrayOf(LoginNavigator::class, RegisterNavigator::class)

    single<MainNavigator> {
        object : MainNavigator {
            override fun navigateToTransactions(context: Context) {
                (context as AppCompatActivity).supportFragmentManager.commit {
                    replace(container, TransactionsFragment())
                }
            }

            override fun navigateToProfile(context: Context) {
                (context as AppCompatActivity).supportFragmentManager.commit {
                    replace(container, ProfileFragment())
                }
            }
        }
    }

    single<SplashNavigator> {
        object : SplashNavigator {
            override fun navigateToLogin(context: Context) {
                val intent = Intent(context, AuthActivity::class.java)
                intent.flags = Intent.FLAG_ACTIVITY_NEW_TASK or Intent.FLAG_ACTIVITY_CLEAR_TASK
                context.startActivity(intent)
            }

            override fun navigateToTransactions(context: Context) {
                val intent = Intent(context, MainActivity::class.java)
                intent.flags = Intent.FLAG_ACTIVITY_NEW_TASK or Intent.FLAG_ACTIVITY_CLEAR_TASK
                context.startActivity(intent)
            }
        }
    }

    single<ProfileNavigator> {
        object : ProfileNavigator {
            override fun navigateToWelcome(context: Context) {
                val intent = Intent(context, AuthActivity::class.java)
                intent.flags = Intent.FLAG_ACTIVITY_NEW_TASK or Intent.FLAG_ACTIVITY_CLEAR_TASK
                context.startActivity(intent)
            }
        }
    }
}
