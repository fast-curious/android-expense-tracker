package com.mexator.expensetracker.di

import android.util.Log
import com.mexator.expensetracker.domain.auth.AuthProvider
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import org.koin.core.qualifier.named
import org.koin.dsl.module

val networkModule = module {
    val logging = HttpLoggingInterceptor {
        Log.i("OkHttp", it)
    }.setLevel(HttpLoggingInterceptor.Level.BODY)

    single(named("authorized")) {
        val tokenProvider: AuthProvider = get()

        OkHttpClient.Builder()
            .addInterceptor { chain ->
                val request = chain.request().newBuilder()
                    .addHeader("Authorization", tokenProvider.token)
                    .build()
                chain.proceed(request)
            }
            .addInterceptor(logging)
            .build()
    }

    single(named("unauthorized")) {
        OkHttpClient.Builder()
            .addInterceptor(logging)
            .build()
    }
}
