package com.mexator.expensetracker.di

import android.content.Context
import android.content.Context.MODE_PRIVATE
import androidx.core.content.edit
import com.mexator.expensetracker.domain.auth.Token
import com.mexator.expensetracker.domain.auth.local.LocalTokenDataSource
import kotlinx.serialization.decodeFromString
import kotlinx.serialization.encodeToString
import kotlinx.serialization.json.Json
import org.koin.android.ext.koin.androidContext
import org.koin.dsl.module
import java.util.*

private const val AUTH_PREFS_NAME = "auth"
private const val AUTH_PREFS_TOKEN_KEY = "token"

val localStorageModule = module {
    single<LocalTokenDataSource> {
        val applicationContext: Context = androidContext()

        object : LocalTokenDataSource {
            private val prefs =
                applicationContext.getSharedPreferences(AUTH_PREFS_NAME, MODE_PRIVATE)

            override suspend fun saveToken(token: Token) {
                prefs.edit(commit = true) {
                    putString(AUTH_PREFS_TOKEN_KEY, Json.encodeToString(token))
                }
            }

            override suspend fun loadToken(): Optional<Token> {
                val tokenString = prefs.getString(AUTH_PREFS_TOKEN_KEY, null)
                return Optional.ofNullable(tokenString?.let { it -> Json.decodeFromString(it) })
            }

            override suspend fun clearToken() {
                prefs.edit(commit = true) {
                    clear()
                }
            }
        }
    }
}
