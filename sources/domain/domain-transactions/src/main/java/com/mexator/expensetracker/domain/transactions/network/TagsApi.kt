package com.mexator.expensetracker.domain.transactions.network

import com.mexator.expensetracker.domain.transactions.model.Tag
import com.mexator.expensetracker.domain.transactions.network.dto.CreateTag
import retrofit2.http.Body
import retrofit2.http.POST

interface TagsApi {
    @POST("/tags/")
    suspend fun createTag(@Body createTag: CreateTag): Tag
}
