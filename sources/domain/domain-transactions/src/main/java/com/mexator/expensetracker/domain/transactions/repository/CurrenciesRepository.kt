package com.mexator.expensetracker.domain.transactions.repository

import com.mexator.expensetracker.domain.transactions.model.Currency
import com.mexator.expensetracker.domain.transactions.model.RUB
import com.mexator.expensetracker.domain.transactions.network.CurrenciesApi
import retrofit2.HttpException

interface CurrenciesRepository {
    suspend fun getCurrencies(): List<Currency>
}

class CurrencyRepositoryImpl(private val currenciesApi: CurrenciesApi) : CurrenciesRepository {
    companion object {
        val defaultCurrencies = listOf(
            RUB,
            Currency("usd", "Us Dollar")
        )
    }

    override suspend fun getCurrencies(): List<Currency> {
        return try {
            currenciesApi.getCurrencies()
        } catch (_: HttpException) {
            defaultCurrencies
        }
    }
}
