package com.mexator.expensetracker.domain.transactions.di

import com.mexator.expensetracker.domain.common.BackendUrl
import com.mexator.expensetracker.domain.common.createRetrofit
import com.mexator.expensetracker.domain.transactions.network.CurrenciesApi
import com.mexator.expensetracker.domain.transactions.network.TagsApi
import com.mexator.expensetracker.domain.transactions.network.TransactionsApi
import com.mexator.expensetracker.domain.transactions.repository.CurrenciesRepository
import com.mexator.expensetracker.domain.transactions.repository.CurrencyRepositoryImpl
import com.mexator.expensetracker.domain.transactions.repository.TagRepository
import com.mexator.expensetracker.domain.transactions.repository.TagRepositoryImpl
import com.mexator.expensetracker.domain.transactions.repository.TransactionsRepository
import com.mexator.expensetracker.domain.transactions.repository.TransactionsRepositoryImpl
import org.koin.core.qualifier.named
import org.koin.dsl.module

fun domainTransactionsModule(backendUrl: BackendUrl) = module {

    single<TransactionsApi> {
        createRetrofit(
            get(named("authorized")),
            backendUrl
        ).create(TransactionsApi::class.java)
    }

    single<TransactionsRepository> { TransactionsRepositoryImpl(get()) }

    single<TagsApi> {
        createRetrofit(
            get(named("authorized")),
            backendUrl
        ).create(TagsApi::class.java)
    }

    single<TagRepository> { TagRepositoryImpl(get()) }

    single<CurrenciesApi> {
        createRetrofit(
            get(named("authorized")),
            backendUrl
        ).create(CurrenciesApi::class.java)
    }
    single<CurrenciesRepository> { CurrencyRepositoryImpl(get()) }
}
