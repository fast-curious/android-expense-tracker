package com.mexator.expensetracker.domain.transactions.model

import com.mexator.expensetracker.domain.common.deserializer.DateDeserializer
import kotlinx.serialization.SerialName
import kotlinx.serialization.Serializable
import java.util.*

@Serializable
data class Transaction(
    val id: Int,
    val currency: Currency,
    val amount: Double,
    @SerialName("base_amount")
    val baseAmount: Double? = null,
    @SerialName("base_currency")
    val baseCurrency: Currency = RUB,
    val name: String,
    val comment: String,
    val tag: Tag?,
    @SerialName("created_at")
    @Serializable(with = DateDeserializer::class)
    val creationDate: Date
)
