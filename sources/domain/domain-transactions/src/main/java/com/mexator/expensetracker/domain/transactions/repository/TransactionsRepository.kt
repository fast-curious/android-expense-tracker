package com.mexator.expensetracker.domain.transactions.repository

import com.mexator.expensetracker.domain.transactions.model.Currency
import com.mexator.expensetracker.domain.transactions.model.Transaction
import com.mexator.expensetracker.domain.transactions.network.TransactionsApi
import com.mexator.expensetracker.domain.transactions.network.dto.CreateTransaction
import com.mexator.expensetracker.domain.transactions.network.dto.UpdateTransaction
import com.mexator.expensetracker.domain.transactions.repository.TransactionsRepository.Companion.PAGE_SIZE

interface TransactionsRepository {
    companion object {
        const val PAGE_SIZE = 15
    }

    suspend fun getTransactions(page: Int): List<Transaction>

    suspend fun createTransaction(currency: Currency, amount: Int, name: String): Transaction

    suspend fun updateTransaction(transactionId: String, tagId: Int): Transaction
}

class TransactionsRepositoryImpl(private val api: TransactionsApi) : TransactionsRepository {

    override suspend fun getTransactions(page: Int): List<Transaction> {
        return api.getTransactions(offset = PAGE_SIZE * page, limit = PAGE_SIZE)
    }

    override suspend fun createTransaction(
        currency: Currency,
        amount: Int,
        name: String
    ): Transaction {
        return api.createTransaction(
            CreateTransaction(
                amount = amount,
                name = name,
                comment = "",
                currencyCode = currency.code
            )
        )
    }

    override suspend fun updateTransaction(transactionId: String, tagId: Int): Transaction {
        return api.updateTransaction(transactionId, UpdateTransaction(tagId = tagId))
    }
}
