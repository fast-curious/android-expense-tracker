package com.mexator.expensetracker.domain.transactions.model

import kotlinx.serialization.Serializable

@Serializable
class Tag(
    val name: String,
    val id: Int
)
