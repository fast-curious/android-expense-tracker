package com.mexator.expensetracker.domain.transactions.network.dto

import kotlinx.serialization.Serializable

@Serializable
class CreateTag(
    val name: String
)
