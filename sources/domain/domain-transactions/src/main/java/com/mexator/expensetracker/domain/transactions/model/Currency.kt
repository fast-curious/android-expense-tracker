package com.mexator.expensetracker.domain.transactions.model

import kotlinx.serialization.Serializable

@Serializable
data class Currency(
    val name: String,
    val code: String
)

val RUB = Currency("Russian Ruble", "rub")
