package com.mexator.expensetracker.domain.transactions.network

import com.mexator.expensetracker.domain.transactions.model.Transaction
import com.mexator.expensetracker.domain.transactions.network.dto.CreateTransaction
import com.mexator.expensetracker.domain.transactions.network.dto.UpdateTransaction
import retrofit2.http.Body
import retrofit2.http.GET
import retrofit2.http.PATCH
import retrofit2.http.POST
import retrofit2.http.Path
import retrofit2.http.Query

interface TransactionsApi {
    @GET("/transactions")
    suspend fun getTransactions(
        @Query("offset") offset: Int,
        @Query("limit") limit: Int
    ): List<Transaction>

    @POST("/transactions")
    suspend fun createTransaction(@Body createTransaction: CreateTransaction): Transaction

    @PATCH("/transactions/{transactionId}")
    suspend fun updateTransaction(
        @Path("transactionId") transactionId: String,
        @Body updateTransaction: UpdateTransaction
    ): Transaction
}
