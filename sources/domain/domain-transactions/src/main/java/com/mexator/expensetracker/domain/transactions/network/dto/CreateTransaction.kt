package com.mexator.expensetracker.domain.transactions.network.dto

import kotlinx.serialization.SerialName
import kotlinx.serialization.Serializable

@Serializable
data class CreateTransaction(
    val amount: Int,
    val name: String,
    val comment: String,
    @SerialName("currency_code")
    val currencyCode: String
)
