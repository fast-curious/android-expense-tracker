package com.mexator.expensetracker.domain.transactions.repository

import com.mexator.expensetracker.domain.transactions.model.Tag
import com.mexator.expensetracker.domain.transactions.network.TagsApi
import com.mexator.expensetracker.domain.transactions.network.dto.CreateTag

interface TagRepository {
    /**
     * @return id of new tag
     */
    suspend fun getOrCreateTag(name: String): Tag
}

class TagRepositoryImpl(private val tagsApi: TagsApi) : TagRepository {
    override suspend fun getOrCreateTag(name: String): Tag {
        return tagsApi.createTag(CreateTag(name))
    }
}
