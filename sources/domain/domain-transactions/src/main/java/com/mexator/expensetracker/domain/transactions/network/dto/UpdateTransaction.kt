package com.mexator.expensetracker.domain.transactions.network.dto

import kotlinx.serialization.EncodeDefault
import kotlinx.serialization.EncodeDefault.Mode.NEVER
import kotlinx.serialization.ExperimentalSerializationApi
import kotlinx.serialization.SerialName
import kotlinx.serialization.Serializable

@OptIn(ExperimentalSerializationApi::class)
@Serializable
data class UpdateTransaction(
    @EncodeDefault(NEVER)
    val amount: Int? = null,
    @EncodeDefault(NEVER)
    val name: String? = null,
    @EncodeDefault(NEVER)
    val comment: String? = null,
    @SerialName("currency_code")
    @EncodeDefault(NEVER)
    val currencyCode: String? = null,
    @SerialName("tag_id")
    @EncodeDefault(NEVER)
    val tagId: Int? = null,
)
