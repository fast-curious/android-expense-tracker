package com.mexator.expensetracker.domain.transactions.network

import com.mexator.expensetracker.domain.transactions.model.Currency
import retrofit2.http.GET

interface CurrenciesApi {
    @GET("/currencies")
    suspend fun getCurrencies(): List<Currency>
}
