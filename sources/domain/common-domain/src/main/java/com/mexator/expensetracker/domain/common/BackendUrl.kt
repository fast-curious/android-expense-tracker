package com.mexator.expensetracker.domain.common

data class BackendUrl(val url: String)

val realUrl = BackendUrl("http://expense-tracker.mexator.xyz")
val testUrl = BackendUrl("http://localhost:5005")
