@file:OptIn(ExperimentalSerializationApi::class)

package com.mexator.expensetracker.domain.common

import com.jakewharton.retrofit2.converter.kotlinx.serialization.asConverterFactory
import kotlinx.serialization.ExperimentalSerializationApi
import kotlinx.serialization.json.Json
import okhttp3.MediaType
import okhttp3.OkHttpClient
import retrofit2.Retrofit

private val json = Json {
    ignoreUnknownKeys = true
}

fun createRetrofit(okHttpClient: OkHttpClient, baseUrl: BackendUrl): Retrofit {
    val contentType = MediaType.get("application/json")
    return Retrofit.Builder()
        .baseUrl(baseUrl.url)
        .client(okHttpClient)
        .addConverterFactory(json.asConverterFactory(contentType))
        .build()
}
