package com.mexator.expensetracker.domain.auth.local

import com.mexator.expensetracker.domain.auth.Token
import java.util.*

interface LocalTokenDataSource {
    suspend fun loadToken(): Optional<Token>
    suspend fun saveToken(token: Token)
    suspend fun clearToken()
}
