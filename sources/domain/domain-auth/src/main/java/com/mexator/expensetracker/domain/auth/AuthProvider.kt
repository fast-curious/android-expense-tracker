package com.mexator.expensetracker.domain.auth

import com.mexator.expensetracker.domain.auth.local.LocalTokenDataSource
import com.mexator.expensetracker.domain.auth.network.AuthApi
import retrofit2.HttpException

interface AuthProvider {
    val token: String
}

internal class AuthProviderImpl(
    private val api: AuthApi,
    private val localTokenDataSource: LocalTokenDataSource
) : AuthProvider, AuthService {
    override lateinit var token: String

    override suspend fun login(credentials: Credentials) {
        val tokenObject = try {
            api.getToken(credentials.username, credentials.password)
        } catch (_: HttpException) {
            throw LoginFailed()
        }
        localTokenDataSource.saveToken(tokenObject)
        token = tokenObject.toString()
    }

    override suspend fun register(credentials: Credentials) {
        api.createUser(credentials)
        login(credentials)
    }

    override suspend fun needLogin(): Boolean {
        val tokenOptional = localTokenDataSource.loadToken()
        if (!tokenOptional.isPresent) return true

        return try {
            token = tokenOptional.get().toString()
            api.getMe(token).username
            false
        } catch (_: HttpException) {
            true
        }
    }

    override suspend fun logout() {
        localTokenDataSource.clearToken()
        token = ""
    }
}
