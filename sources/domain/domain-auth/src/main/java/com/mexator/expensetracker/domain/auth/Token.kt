package com.mexator.expensetracker.domain.auth

import kotlinx.serialization.SerialName
import kotlinx.serialization.Serializable

@Serializable
data class Token(
    @SerialName("access_token")
    val accessToken: String,
    @SerialName("token_type")
    val tokenType: String,
) {
    override fun toString(): String {
        val tokenType = tokenType.replaceFirstChar(Char::uppercaseChar)
        return "$tokenType $accessToken"
    }
}
