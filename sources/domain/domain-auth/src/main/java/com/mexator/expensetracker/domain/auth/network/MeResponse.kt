package com.mexator.expensetracker.domain.auth.network

import kotlinx.serialization.Serializable

@Serializable
internal data class MeResponse(
    val username: String
)
