package com.mexator.expensetracker.domain.auth

class LoginFailed : Exception()

interface AuthService {
    suspend fun login(credentials: Credentials)

    suspend fun register(credentials: Credentials)

    suspend fun needLogin(): Boolean

    suspend fun logout()
}
