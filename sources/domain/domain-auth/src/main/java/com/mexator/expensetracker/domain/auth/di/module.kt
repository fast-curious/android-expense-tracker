package com.mexator.expensetracker.domain.auth.di

import com.mexator.expensetracker.domain.auth.AuthProvider
import com.mexator.expensetracker.domain.auth.AuthProviderImpl
import com.mexator.expensetracker.domain.auth.AuthService
import com.mexator.expensetracker.domain.auth.network.AuthApi
import com.mexator.expensetracker.domain.common.BackendUrl
import com.mexator.expensetracker.domain.common.createRetrofit
import org.koin.core.qualifier.named
import org.koin.dsl.binds
import org.koin.dsl.module

val domainAuthModule = module {
    val backendUrl = BackendUrl("http://expense-tracker.mexator.xyz")

    single<AuthApi> {
        createRetrofit(get(named("unauthorized")), backendUrl)
            .create(AuthApi::class.java)
    }

    single {
        AuthProviderImpl(get(), get())
    } binds arrayOf(AuthService::class, AuthProvider::class)
}
