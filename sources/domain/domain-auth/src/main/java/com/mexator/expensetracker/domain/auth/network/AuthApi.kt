package com.mexator.expensetracker.domain.auth.network

import com.mexator.expensetracker.domain.auth.Credentials
import com.mexator.expensetracker.domain.auth.Token
import retrofit2.http.Body
import retrofit2.http.Field
import retrofit2.http.FormUrlEncoded
import retrofit2.http.GET
import retrofit2.http.Header
import retrofit2.http.POST

internal interface AuthApi {
    @FormUrlEncoded
    @POST("/token")
    suspend fun getToken(
        @Field("username") username: String,
        @Field("password") password: String
    ): Token

    @POST("/user")
    suspend fun createUser(
        @Body credentials: Credentials
    )

    @GET("/user/me/")
    suspend fun getMe(@Header("Authorization") token: String): MeResponse
}
