package com.mexator.expensetracker.feature.transactions.ui

import android.content.DialogInterface
import android.os.Bundle
import android.view.View
import android.widget.AutoCompleteTextView
import android.widget.EditText
import androidx.appcompat.app.AlertDialog
import androidx.fragment.app.Fragment
import androidx.lifecycle.MutableLiveData
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.google.android.material.snackbar.Snackbar
import com.mexator.expensetracker.common.feature.ContentState
import com.mexator.expensetracker.common.feature.PageState
import com.mexator.expensetracker.domain.transactions.model.Currency
import com.mexator.expensetracker.feature.transactions.R
import com.mexator.expensetracker.feature.transactions.databinding.FragmentTransactionsBinding
import com.mexator.expensetracker.feature.transactions.presentation.News
import com.mexator.expensetracker.feature.transactions.presentation.News.Error
import com.mexator.expensetracker.feature.transactions.presentation.News.NavigateToEnterTagName
import com.mexator.expensetracker.feature.transactions.presentation.News.ScrollListToTop
import com.mexator.expensetracker.feature.transactions.presentation.State
import com.mexator.expensetracker.feature.transactions.presentation.ViewModel
import com.mexator.expensetracker.feature.transactions.ui.TransactionListUi.EmptyUi
import com.mexator.expensetracker.feature.transactions.ui.TransactionListUi.ErrorUi
import com.mexator.expensetracker.feature.transactions.ui.TransactionListUi.LoadingUi
import com.mexator.expensetracker.feature.transactions.ui.TransactionListUi.PageErrorUi
import com.mexator.expensetracker.feature.transactions.ui.TransactionListUi.PageLoadingUi
import com.mexator.expensetracker.feature.transactions.ui.TransactionsAdapter.RetryClickListener
import com.mexator.expensetracker.feature.transactions.ui.mapper.TransactionsUiMapper
import com.mexator.expensetracker.feature.transactions.ui.recycler.CurrenciesDropdownAdapter
import com.mexator.expensetracker.feature.transactions.ui.recycler.ViewHolder.TransactionViewHolder.TransactionClickListener
import org.koin.androidx.viewmodel.ext.android.viewModel

private const val PRELOAD_MARGIN = 10

class TransactionsFragment : Fragment(R.layout.fragment_transactions) {

    private val viewModel: ViewModel by viewModel()
    private lateinit var binding: FragmentTransactionsBinding

    private val listener = object : RetryClickListener, TransactionClickListener {
        override fun onRetryAllClick() {
            viewModel.init()
        }

        override fun onRetryPageClick() {
            viewModel.getNextPage()
        }

        override fun onChipClicked(transactionId: String) {
            viewModel.onChipClicked(transactionId)
        }
    }
    private val adapter: TransactionsAdapter = TransactionsAdapter(listener, listener)
    private val mapper: TransactionsUiMapper by lazy {
        TransactionsUiMapper(requireContext())
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        binding = FragmentTransactionsBinding.bind(view)
        binding.transactionsList.adapter = adapter

        viewModel.getState().observe(viewLifecycleOwner, ::render)
        viewModel.getNews().observe(viewLifecycleOwner, ::handleNews)
        setListeners()
        viewModel.init()
    }

    private fun setListeners() {
        with(binding.createTransactionFields) {
            val currencyValue = MutableLiveData<Currency>()
            (currencyInput.editText as AutoCompleteTextView)
                .setOnItemClickListener { parent, _, position, _ ->
                    val cur = parent.getItemAtPosition(position) as Currency
                    currencyValue.value = cur
                }
            createButton.setOnClickListener {
                val name = nameEdittext.text.toString()
                val amount = amountEdittext.text.toString()
                viewModel.createTransaction(name, amount, currencyValue.value)
            }
        }

        binding.transactionsList.addOnScrollListener(object : RecyclerView.OnScrollListener() {
            override fun onScrolled(recyclerView: RecyclerView, dx: Int, dy: Int) {
                super.onScrolled(recyclerView, dx, dy)
                val layoutManager = recyclerView.layoutManager as LinearLayoutManager
                val lastPosition = layoutManager.findLastVisibleItemPosition()
                val totalItems = layoutManager.itemCount
                if (totalItems - lastPosition <= PRELOAD_MARGIN) {
                    viewModel.getNextPage()
                }
            }
        })
    }

    private fun render(state: State) {
        with(binding.createTransactionFields) {
            val currencyValues = state.createTransactionState.currencies
            (currencyInput.editText as AutoCompleteTextView).setAdapter(
                CurrenciesDropdownAdapter(
                    requireContext(),
                    R.layout.item_currency_list,
                    currencyValues
                )
            )
        }
        val itemsList = when (state.transactions) {
            is ContentState.Loading -> listOf(LoadingUi)
            is ContentState.Error -> listOf(
                ErrorUi(
                    resources.getString(
                        R.string.message_error_receiving_transactions,
                        state.transactions.exception.message
                    )
                )
            )
            is ContentState.Content -> state.transactions.content.map(mapper::map)
        }.toMutableList()

        when (state.nextPageState) {
            is PageState.Loading -> itemsList.add(PageLoadingUi)
            is PageState.Error -> itemsList.add(PageErrorUi)
            else -> Unit
        }

        if (itemsList.isEmpty()) itemsList.add(EmptyUi)

        adapter.submitList(itemsList)
    }

    private fun handleNews(news: News) {
        when (news) {
            is Error -> Snackbar
                .make(binding.root, news.message, Snackbar.LENGTH_SHORT)
                .show()
            ScrollListToTop -> binding.transactionsList.scrollToPosition(0)
            is NavigateToEnterTagName -> {
                enterTagDialog { text ->
                    viewModel.newTagEntered(
                        news.transactionId,
                        text
                    )
                }.show()
            }
        }
    }

    private fun enterTagDialog(actionSubmit: (String) -> Unit): AlertDialog.Builder {
        val view = layoutInflater.inflate(R.layout.dialog_enter_text, null)
        val listener = DialogInterface.OnClickListener { _, _ ->
            actionSubmit(view.findViewById<EditText>(R.id.textInput).text.toString())
        }

        return AlertDialog.Builder(requireContext())
            .setTitle(R.string.transactions_dialog_add_tag_title)
            .setPositiveButton(R.string.transactions_dialog_add_tag_button_submit, listener)
            .setNegativeButton(R.string.transactions_dialog_add_tag_button_cancel, null)
            .setView(view)
    }
}

