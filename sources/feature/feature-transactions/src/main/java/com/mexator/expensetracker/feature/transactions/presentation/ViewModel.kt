package com.mexator.expensetracker.feature.transactions.presentation

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.mexator.expensetracker.common.feature.ContentState
import com.mexator.expensetracker.common.feature.PageState
import com.mexator.expensetracker.common.feature.mapValue
import com.mexator.expensetracker.common.feature.requireValue
import com.mexator.expensetracker.domain.transactions.model.Currency
import com.mexator.expensetracker.domain.transactions.repository.CurrenciesRepository
import com.mexator.expensetracker.domain.transactions.repository.TagRepository
import com.mexator.expensetracker.domain.transactions.repository.TransactionsRepository
import com.mexator.expensetracker.feature.transactions.presentation.News.Error
import com.mexator.expensetracker.feature.transactions.presentation.News.NavigateToEnterTagName
import com.mexator.expensetracker.feature.transactions.presentation.News.ScrollListToTop
import kotlinx.coroutines.launch
import kotlinx.coroutines.sync.Mutex
import kotlinx.coroutines.sync.withLock

internal class ViewModel(
    private val transactionsRepository: TransactionsRepository,
    private val tagRepository: TagRepository,
    private val currenciesRepository: CurrenciesRepository
) : ViewModel() {

    private val state: MutableLiveData<State> = MutableLiveData<State>().also {
        it.value = State(
            CreateTransactionState(),
            ContentState.Content(emptyList()),
            PageState.None,
            0
        )
    }
    private val news: MutableLiveData<News> = MutableLiveData()

    fun getState(): LiveData<State> {
        return state
    }

    fun getNews(): LiveData<News> {
        return news
    }

    private val fetchPageMutex: Mutex = Mutex()
    fun init() {
        viewModelScope.launch {
            fetchPageMutex.withLock {
                state.mapValue {
                    copy(
                        transactions = ContentState.Loading,
                        nextPageState = PageState.None,
                        currentPage = 0
                    )
                }
                val newContent = kotlin.runCatching {
                    transactionsRepository.getTransactions(0)
                }.fold(
                    { ContentState.Content(it) },
                    { ContentState.Error(it) }
                )
                state.mapValue {
                    val page = if (newContent is ContentState.Error) 0 else 1
                    copy(transactions = newContent, currentPage = page)
                }

            }
        }
        viewModelScope.launch {
            val currencies = currenciesRepository.getCurrencies()
            state.mapValue {
                copy(
                    createTransactionState = CreateTransactionState(
                        currencies = currencies
                    )
                )
            }
        }
    }

    fun getNextPage() {
        if (fetchPageMutex.isLocked) return
        if (state.value?.nextPageState == PageState.NoMore) return

        viewModelScope.launch {
            fetchPageMutex.withLock {
                state.mapValue {
                    copy(nextPageState = PageState.Loading)
                }
                kotlin.runCatching {
                    transactionsRepository.getTransactions(state.requireValue().currentPage)
                }.map { transactions ->
                    val currentState = state.requireValue()
                    val oldContent = (currentState.transactions as ContentState.Content).content
                    val hasNextPage = transactions.size < TransactionsRepository.PAGE_SIZE
                    currentState.copy(
                        transactions = ContentState.Content(oldContent + transactions),
                        nextPageState = if (hasNextPage) PageState.NoMore else PageState.None,
                        currentPage = currentState.currentPage + 1
                    )
                }.fold(
                    { state.value = it },
                    { state.mapValue { copy(nextPageState = PageState.Error(it)) } }
                )
            }
        }
    }

    fun createTransaction(name: String, amountStr: String, currency: Currency?) {
        if (name.isBlank()) {
            news.value = Error("New transaction's name is empty!")
            return
        }
        if (amountStr.isBlank()) {
            news.value = Error("New transaction's amount is empty!")
            return
        }
        if (currency == null) {
            news.value = Error("Currency is not selected!")
            return
        }
        val amount = try {
            amountStr.toInt()
        } catch (ex: NumberFormatException) {
            news.value = Error("New transaction's amount is not a number!")
            return
        }

        viewModelScope.launch {
            kotlin.runCatching {
                transactionsRepository.createTransaction(
                    currency,
                    amount,
                    name
                )
            }.map {
                val currentState = state.requireValue()
                if (currentState.transactions is ContentState.Content) {
                    ContentState.Content(listOf(it) + currentState.transactions.content)
                } else currentState.transactions
            }.fold(
                {
                    state.mapValue { copy(transactions = it) }
                    news.postValue(ScrollListToTop)
                },
                { news.value = Error("Error creating new transaction: ${it.message}") }
            )
        }
    }

    fun onChipClicked(transactionId: String) {
        news.value = NavigateToEnterTagName(transactionId)
    }

    fun newTagEntered(transactionId: String, name: String) {
        viewModelScope.launch {
            name.runCatching { tagRepository.getOrCreateTag(this) }
                .map { tag -> transactionsRepository.updateTransaction(transactionId, tag.id) }
                .fold(
                    { transaction ->
                        state.mapValue {
                            val oldContent = (transactions as ContentState.Content).content
                            val newContent =
                                oldContent.map { if (it.id == transaction.id) transaction else it }
                            copy(transactions = ContentState.Content(newContent))
                        }
                    }, { error ->
                        news.value = Error("Error while creating tag: ${error.message}")
                    }
                )
        }
    }
}
