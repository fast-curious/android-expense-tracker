package com.mexator.expensetracker.feature.transactions.ui.mapper

import android.content.Context
import androidx.core.content.res.ResourcesCompat
import com.mexator.expensetracker.feature.transactions.R

class TagColorProvider(private val context: Context) {
    private val colors = context.resources.obtainTypedArray(R.array.tag_colors)
    private val defaultColor = ResourcesCompat.getColor(
        context.resources,
        R.color.tag_create_color,
        context.theme
    )

    fun getColorForId(id: Int?): Int {
        if (id == null) return defaultColor
        return colors.getColor(id % colors.length(), defaultColor)
    }
}
