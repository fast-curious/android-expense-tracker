package com.mexator.expensetracker.feature.transactions.presentation

import com.mexator.expensetracker.common.feature.ContentState
import com.mexator.expensetracker.common.feature.PageState
import com.mexator.expensetracker.domain.transactions.model.Currency
import com.mexator.expensetracker.domain.transactions.model.Transaction

internal data class State(
    val createTransactionState: CreateTransactionState,
    val transactions: ContentState<List<Transaction>>,
    val nextPageState: PageState,
    val currentPage: Int = 0
)

internal data class CreateTransactionState(
    val currencies: List<Currency> = listOf()
)
