package com.mexator.expensetracker.feature.transactions.ui

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import com.mexator.expensetracker.feature.transactions.R
import com.mexator.expensetracker.feature.transactions.ui.TransactionListUi.ErrorUi
import com.mexator.expensetracker.feature.transactions.ui.TransactionListUi.TransactionUi
import com.mexator.expensetracker.feature.transactions.ui.recycler.ViewHolder
import com.mexator.expensetracker.feature.transactions.ui.recycler.ViewHolder.ErrorViewHolder
import com.mexator.expensetracker.feature.transactions.ui.recycler.ViewHolder.LoadingViewHolder
import com.mexator.expensetracker.feature.transactions.ui.recycler.ViewHolder.NoItemsViewHolder
import com.mexator.expensetracker.feature.transactions.ui.recycler.ViewHolder.PageErrorViewHolder
import com.mexator.expensetracker.feature.transactions.ui.recycler.ViewHolder.PageLoadingViewHolder
import com.mexator.expensetracker.feature.transactions.ui.recycler.ViewHolder.TransactionViewHolder

class TransactionsAdapter(
    private val transactionClickListener: TransactionViewHolder.TransactionClickListener,
    private val retryClickListener: RetryClickListener
) :
    ListAdapter<TransactionListUi, ViewHolder>(TransactionsDiffer) {

    override fun getItemViewType(position: Int): Int = getItem(position).type

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val view = LayoutInflater.from(parent.context).inflate(viewType, parent, false)

        return when (viewType) {
            R.layout.item_transactions_loading -> LoadingViewHolder(view)
            R.layout.item_transactions_empty -> NoItemsViewHolder(view)
            R.layout.item_transactions_error -> ErrorViewHolder(retryClickListener, view)
            R.layout.item_transactions_transaction -> TransactionViewHolder(
                view,
                transactionClickListener
            )
            R.layout.item_transactions_page_loading -> PageLoadingViewHolder(view)
            R.layout.item_transactions_page_error -> PageErrorViewHolder(
                retryClickListener,
                view
            )
            else -> error("Unknown viewType: $viewType")
        }
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val item = getItem(position)
        when (holder) {
            is ErrorViewHolder -> holder.bind(item as ErrorUi)
            is TransactionViewHolder -> holder.bind(item as TransactionUi)
            else -> Unit
        }
    }

    interface RetryClickListener {
        fun onRetryAllClick()

        fun onRetryPageClick()
    }
}

private object TransactionsDiffer : DiffUtil.ItemCallback<TransactionListUi>() {
    override fun areItemsTheSame(oldItem: TransactionListUi, newItem: TransactionListUi): Boolean {
        return oldItem.id == newItem.id
    }

    override fun areContentsTheSame(
        oldItem: TransactionListUi,
        newItem: TransactionListUi
    ): Boolean {
        return if (oldItem is TransactionUi && newItem is TransactionUi) {
            oldItem == newItem
        } else if (oldItem is ErrorUi && newItem is ErrorUi) {
            oldItem.message == newItem.message
        } else {
            // Other items are one of a kind
            true
        }
    }
}
