package com.mexator.expensetracker.feature.transactions.ui.recycler

import android.content.res.ColorStateList
import android.view.View
import androidx.recyclerview.widget.RecyclerView
import com.mexator.expensetracker.feature.transactions.databinding.ItemTransactionsErrorBinding
import com.mexator.expensetracker.feature.transactions.databinding.ItemTransactionsPageErrorBinding
import com.mexator.expensetracker.feature.transactions.databinding.ItemTransactionsTransactionBinding
import com.mexator.expensetracker.feature.transactions.ui.TransactionListUi
import com.mexator.expensetracker.feature.transactions.ui.TransactionsAdapter

sealed class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
    class NoItemsViewHolder(itemView: View) : ViewHolder(itemView)
    class LoadingViewHolder(itemView: View) : ViewHolder(itemView)
    class PageLoadingViewHolder(itemView: View) : ViewHolder(itemView)

    class PageErrorViewHolder(
        retryClickListener: TransactionsAdapter.RetryClickListener,
        itemView: View
    ) : ViewHolder(itemView) {
        private val binding = ItemTransactionsPageErrorBinding.bind(itemView)

        init {
            binding.retryButton.setOnClickListener { retryClickListener.onRetryPageClick() }
        }
    }

    class ErrorViewHolder(
        retryClickListener: TransactionsAdapter.RetryClickListener,
        itemView: View
    ) : ViewHolder(itemView) {
        private val binding = ItemTransactionsErrorBinding.bind(itemView)

        init {
            binding.retryButton.setOnClickListener { retryClickListener.onRetryAllClick() }
        }

        fun bind(errorUi: TransactionListUi.ErrorUi) {
            binding.errorMessage.text = errorUi.message
        }
    }

    class TransactionViewHolder(
        itemView: View,
        private val listener: TransactionClickListener
    ) : ViewHolder(itemView) {
        interface TransactionClickListener {
            fun onChipClicked(transactionId: String)
        }

        private val binding = ItemTransactionsTransactionBinding.bind(itemView)
        fun bind(transaction: TransactionListUi.TransactionUi) {
            binding.transactionName.text = transaction.name
            binding.amount.text = transaction.amount
            binding.baseAmount.text = transaction.baseAmount
            binding.tag.chipIcon = transaction.tag.icon
            binding.tag.text = transaction.tag.text
            binding.tag.chipBackgroundColor =
                ColorStateList(arrayOf(intArrayOf()), intArrayOf(transaction.tag.bgColor))

            binding.tag.setOnClickListener { listener.onChipClicked(transaction.id) }
        }
    }
}
