package com.mexator.expensetracker.feature.transactions.ui.mapper

import android.content.Context
import android.graphics.drawable.Drawable
import androidx.core.content.res.ResourcesCompat.getDrawable
import com.mexator.expensetracker.domain.transactions.model.Transaction
import com.mexator.expensetracker.feature.transactions.R
import com.mexator.expensetracker.feature.transactions.ui.TransactionListUi.TagUi
import com.mexator.expensetracker.feature.transactions.ui.TransactionListUi.TransactionUi

class TransactionsUiMapper(private val context: Context) {
    private val tagColorProvider = TagColorProvider(context)

    fun map(transaction: Transaction): TransactionUi {
        val tagDrawable: Drawable? = if (transaction.tag == null) {
            getDrawable(context.resources, R.drawable.ic_add_24, context.theme)
        } else {
            null
        }

        val tagText = transaction.tag?.name
            ?: context.resources.getString(R.string.transactions_add_tag_chip)

        val needBaseAmount = transaction.currency != transaction.baseCurrency
                && transaction.baseAmount != null
        val baseAmount = if (needBaseAmount) {
            "${transaction.baseAmount?.toInt()} ${transaction.baseCurrency.code}"
        } else null

        return TransactionUi(
            transaction.id.toString(),
            transaction.name,
            "${transaction.amount.toInt()} ${transaction.currency.code}",
            baseAmount,
            TagUi(tagText, tagDrawable, tagColorProvider.getColorForId(transaction.tag?.id))
        )
    }
}
