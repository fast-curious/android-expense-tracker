package com.mexator.expensetracker.feature.transactions.ui.recycler

import android.content.Context
import android.view.View
import android.view.ViewGroup
import android.widget.ArrayAdapter
import android.widget.Filter
import android.widget.TextView
import androidx.annotation.LayoutRes
import com.mexator.expensetracker.domain.transactions.model.Currency

class CurrenciesDropdownAdapter(
    context: Context,
    @LayoutRes resource: Int,
    currencies: List<Currency>
) :
    ArrayAdapter<Currency>(context, resource, currencies) {
    override fun getView(position: Int, convertView: View?, parent: ViewGroup): View {
        val superView = super.getView(position, convertView, parent)
        (superView as TextView).text = getItem(position)?.code
        return superView
    }

    class CurrenciesFilter : Filter() {
        override fun performFiltering(constraint: CharSequence?): FilterResults {
            return FilterResults()
        }

        override fun publishResults(constraint: CharSequence?, results: FilterResults?) = Unit

        override fun convertResultToString(resultValue: Any?): CharSequence {
            if (resultValue is Currency) return resultValue.code
            return super.convertResultToString(resultValue)
        }
    }

    override fun getFilter(): Filter {
        return CurrenciesFilter()
    }
}
