package com.mexator.expensetracker.feature.transactions.ui

import android.graphics.drawable.Drawable
import com.mexator.expensetracker.feature.transactions.R

sealed class TransactionListUi(val id: String, val type: Int) {
    object EmptyUi : TransactionListUi("EmptyUi", R.layout.item_transactions_empty)
    object LoadingUi : TransactionListUi("LoadingUi", R.layout.item_transactions_loading)
    class ErrorUi(val message: String?) :
        TransactionListUi("ErrorUi", R.layout.item_transactions_error)

    data class TagUi(
        val text: String,
        val icon: Drawable?,
        val bgColor: Int,
    )

    data class TransactionUi(
        val transactionId: String,
        val name: String,
        val amount: String,
        val baseAmount: String?,
        val tag: TagUi
    ) : TransactionListUi(transactionId, R.layout.item_transactions_transaction)

    object PageLoadingUi : TransactionListUi(
        "PageLoadingUi",
        R.layout.item_transactions_page_loading
    )

    object PageErrorUi : TransactionListUi(
        "PageErrorUi",
        R.layout.item_transactions_page_error
    )
}
