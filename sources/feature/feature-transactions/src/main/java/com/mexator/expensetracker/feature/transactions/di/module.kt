package com.mexator.expensetracker.feature.transactions.di

import com.mexator.expensetracker.feature.transactions.presentation.ViewModel
import org.koin.androidx.viewmodel.dsl.viewModel
import org.koin.dsl.module

val featureTransactionsModule = module {
    viewModel {
        ViewModel(
            transactionsRepository = get(),
            tagRepository = get(),
            currenciesRepository = get()
        )
    }
}
