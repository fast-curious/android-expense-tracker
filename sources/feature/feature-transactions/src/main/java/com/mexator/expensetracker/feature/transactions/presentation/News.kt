package com.mexator.expensetracker.feature.transactions.presentation

sealed class News {
    class Error(val message: String) : News()
    object ScrollListToTop : News()

    class NavigateToEnterTagName(val transactionId: String) : News()
}
