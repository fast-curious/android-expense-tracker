package com.mexator.expensetracker.common.feature

import androidx.lifecycle.LiveData
import androidx.lifecycle.MediatorLiveData
import androidx.lifecycle.MutableLiveData

fun <T : Any> MutableLiveData<T>.mapValue(block: T.() -> T) {
    requireNotNull(value)
    value = (value as T).block()
}

fun <T : Any> LiveData<T>.requireValue(): T {
    return requireNotNull(value)
}

fun <A : Any, B : Any> zip(first: LiveData<A>, second: LiveData<B>): LiveData<Pair<A, B>> {
    val mediatorLiveData = MediatorLiveData<Pair<A, B>>()

    var firstValue: A? = null
    var secondValue: B? = null

    mediatorLiveData.addSource(first) {
        firstValue = it
        if (secondValue != null) {
            mediatorLiveData.value = Pair(firstValue!!, secondValue!!)
        }
    }
    mediatorLiveData.addSource(second) {
        secondValue = it
        if (firstValue != null) {
            mediatorLiveData.value = Pair(firstValue!!, secondValue!!)
        }
    }

    return mediatorLiveData
}

fun <A : Any, B : Any, C : Any, D : Any> zip(
    first: LiveData<A>,
    second: LiveData<B>,
    third: LiveData<C>,
    transform: (A, B, C) -> D
): LiveData<D> {
    val mediatorLiveData = MediatorLiveData<D>()

    var firstValue: A? = null
    var secondValue: B? = null
    var thirdValue: C? = null

    fun update() {
        val localFirstValue = firstValue
        val localSecondValue = secondValue
        val localThirdValue = thirdValue
        if (localFirstValue != null && localSecondValue != null && localThirdValue != null)
            mediatorLiveData.value = transform(localFirstValue, localSecondValue, localThirdValue)
    }

    mediatorLiveData.addSource(first) {
        firstValue = it
        update()
    }
    mediatorLiveData.addSource(second) {
        secondValue = it
        update()
    }
    mediatorLiveData.addSource(third) {
        thirdValue = it
        update()
    }
    return mediatorLiveData
}
