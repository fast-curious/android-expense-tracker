package com.mexator.expensetracker.common.feature

sealed class PageState {
    /**
     * First page not loaded yet
     */
    object None : PageState()

    /**
     * Loading next page
     */
    object Loading : PageState()

    /**
     * Error loading next page
     */
    class Error(val error: Throwable) : PageState()

    /**
     * No more pages
     */
    object NoMore : PageState()
}
