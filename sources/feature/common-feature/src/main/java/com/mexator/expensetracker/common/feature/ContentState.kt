package com.mexator.expensetracker.common.feature

sealed class ContentState<out T : Any> {
    object Loading : ContentState<Nothing>() {
        override fun toString(): String {
            return "Loading"
        }
    }

    data class Error(val exception: Throwable) : ContentState<Nothing>()
    data class Content<T : Any>(val content: T) : ContentState<T>()
}
