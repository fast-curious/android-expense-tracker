package com.mexator.expensetracker.common.feature.ui

import android.text.TextPaint
import android.text.style.ClickableSpan
import android.view.View
import androidx.annotation.ColorInt

class ClickableSpan(
    @ColorInt
    private val color: Int,
    private val click: (View) -> Unit
) : ClickableSpan() {

    override fun onClick(widget: View) {
        widget.invalidate()
        click(widget)
    }

    override fun updateDrawState(ds: TextPaint) {
        super.updateDrawState(ds)
        ds.isUnderlineText = false
        ds.color = color
    }
}
