package com.mexator.expensetracker.common.feature.ui

import android.text.Editable
import android.text.TextWatcher
import androidx.lifecycle.LiveData

class LiveDataTextWatcher: TextWatcher, LiveData<String>() {
    init {
        value = ""
    }

    override fun beforeTextChanged(s: CharSequence, start: Int, count: Int, after: Int) = Unit

    override fun onTextChanged(s: CharSequence, start: Int, before: Int, count: Int) = Unit

    override fun afterTextChanged(s: Editable) {
        value = s.toString()
    }
}
