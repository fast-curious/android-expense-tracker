package com.mexator.expensetracker.feature.profile.navigation

import android.content.Context

interface ProfileNavigator {
    fun navigateToWelcome(context: Context)
}
