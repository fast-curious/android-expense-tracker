package com.mexator.expensetracker.feature.profile.ui

import android.os.Bundle
import android.view.MenuItem
import android.view.View
import androidx.fragment.app.Fragment
import androidx.lifecycle.lifecycleScope
import com.mexator.expensetracker.domain.auth.AuthService
import com.mexator.expensetracker.feature.profile.R
import com.mexator.expensetracker.feature.profile.databinding.FragmentProfileBinding
import com.mexator.expensetracker.feature.profile.navigation.ProfileNavigator
import kotlinx.coroutines.launch
import org.koin.android.ext.android.inject

class ProfileFragment : Fragment(R.layout.fragment_profile) {
    private val navigator: ProfileNavigator by inject()
    private val authService: AuthService by inject()
    private lateinit var binding: FragmentProfileBinding

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        binding = FragmentProfileBinding.bind(view)
        binding.profileToolbar.setOnMenuItemClickListener(::handleToolbarAction)
    }

    private fun handleToolbarAction(item: MenuItem): Boolean {
        return when (item.itemId) {
            R.id.action_logout -> {
                viewLifecycleOwner.lifecycleScope.launch {
                    authService.logout()
                    navigator.navigateToWelcome(requireContext())
                }
                true
            }
            else -> super.onOptionsItemSelected(item)
        }
    }
}
