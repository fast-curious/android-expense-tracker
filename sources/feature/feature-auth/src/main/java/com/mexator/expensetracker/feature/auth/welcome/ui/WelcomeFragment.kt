package com.mexator.expensetracker.feature.auth.welcome.ui

import android.os.Bundle
import android.text.method.LinkMovementMethod
import android.view.View
import androidx.fragment.app.Fragment
import com.mexator.expensetracker.feature.auth.R
import com.mexator.expensetracker.feature.auth.StringProvider
import com.mexator.expensetracker.feature.auth.databinding.FragmentWelcomeBinding
import com.mexator.expensetracker.feature.auth.welcome.navigation.WelcomeNavigator
import org.koin.android.ext.android.inject

class WelcomeFragment : Fragment(R.layout.fragment_welcome) {
    private val navigator: WelcomeNavigator by inject()
    private lateinit var binding: FragmentWelcomeBinding
    private val stringProvider by lazy { StringProvider(requireContext()) }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        binding = FragmentWelcomeBinding.bind(view)

        binding.buttonLogin.text = stringProvider.getLoginLabelForWelcome { onLoginClicked() }
        binding.buttonLogin.movementMethod = LinkMovementMethod.getInstance()
        binding.buttonRegister.setOnClickListener { onRegisterClicked() }
    }

    private fun onLoginClicked() {
        navigator.navigateToLogin(requireContext())
    }

    private fun onRegisterClicked() {
        navigator.navigateToSignIn(requireContext())
    }
}
