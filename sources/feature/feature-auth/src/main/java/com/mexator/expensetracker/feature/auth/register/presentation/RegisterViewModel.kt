package com.mexator.expensetracker.feature.auth.register.presentation

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.mexator.expensetracker.common.feature.mapValue
import com.mexator.expensetracker.domain.auth.AuthService
import com.mexator.expensetracker.domain.auth.Credentials
import com.mexator.expensetracker.feature.auth.register.model.CredentialField.Confirmation
import com.mexator.expensetracker.feature.auth.register.model.CredentialField.Login
import com.mexator.expensetracker.feature.auth.register.model.ValidationResult.Ok
import com.mexator.expensetracker.feature.auth.register.model.ValidationResult.Wrong
import kotlinx.coroutines.launch

const val MIN_USERNAME_LEN = 4

internal class RegisterViewModel(private val authService: AuthService) : ViewModel() {

    private val state = MutableLiveData<RegisterState>()
        .also { it.value = RegisterState(isLoading = false, Ok) }
    private val news = MutableLiveData<RegisterNews>()

    fun getState(): LiveData<RegisterState> = state
    fun getNews(): LiveData<RegisterNews> = news

    fun register(username: String, password: String, confirmation: String) {
        viewModelScope.launch {
            if (!validateUsername(username)) {
                state.mapValue { copy(validationResult = Wrong(Login)) }
                return@launch
            }
            if (!validatePassword(password, confirmation)) {
                state.mapValue { copy(validationResult = Wrong(Confirmation)) }
                return@launch
            }
            state.mapValue { copy(isLoading = true, validationResult = Ok) }

            kotlin.runCatching {
                authService.register(Credentials(username, password))
            }.fold(onSuccess = {
                news.value = RegisterNews.SuccessfullyRegistered
            }, onFailure = {
                news.value = RegisterNews.LoginError
            })
            state.mapValue { copy(isLoading = false) }
        }
    }

    private fun validateUsername(username: String): Boolean {
        return username.length > MIN_USERNAME_LEN
    }

    private fun validatePassword(password: String, confirmation: String): Boolean {
        return password == confirmation
    }
}
