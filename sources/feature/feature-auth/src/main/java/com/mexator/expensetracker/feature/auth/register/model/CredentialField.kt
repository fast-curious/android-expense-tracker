package com.mexator.expensetracker.feature.auth.register.model

enum class CredentialField {
    Login, Password, Confirmation
}
