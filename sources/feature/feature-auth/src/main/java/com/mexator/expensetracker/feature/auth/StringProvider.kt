package com.mexator.expensetracker.feature.auth

import android.content.Context
import android.graphics.Typeface.BOLD
import android.text.SpannableStringBuilder
import android.text.Spanned
import android.text.style.StyleSpan
import android.view.View
import com.mexator.expensetracker.common.feature.ui.ClickableSpan

internal class StringProvider(private val context: Context) {
    fun getLoginLabelForWelcome(onClickAction: (View) -> Unit): CharSequence {
        return SpannableStringBuilder().apply {
            val part1 = context.resources.getString(R.string.welcome_button_login_part_1)
            val part2 = context.resources.getString(R.string.welcome_button_login_part_2)
            append(part1)
            append(" ")
            append(part2)

            listOf(
                ClickableSpan(
                    context.getColor(R.color.text_primary_on_dark),
                    onClickAction
                ),
                StyleSpan(BOLD),
            ).forEach {
                setSpan(
                    it,
                    part1.length + 1,
                    length,
                    Spanned.SPAN_EXCLUSIVE_EXCLUSIVE
                )
            }
        }
    }

    fun getLoginLabelForRegister(onClickAction: (View) -> Unit): CharSequence {
        return SpannableStringBuilder().apply {
            val part1 = context.resources.getString(R.string.register_offer_login_part_1)
            val part2 = context.resources.getString(R.string.register_offer_login_part_2)
            append(part1)
            append(" ")
            append(part2)

            setSpan(
                ClickableSpan(
                    context.getColor(R.color.text_link), onClickAction
                ),
                part1.length + 1,
                length,
                Spanned.SPAN_EXCLUSIVE_EXCLUSIVE
            )
        }
    }

    fun getRegisterLabelForLogin(onClickAction: (View) -> Unit): CharSequence {
        return SpannableStringBuilder().apply {
            val part1 = context.resources.getString(R.string.login_offer_register_part_1)
            val part2 = context.resources.getString(R.string.login_offer_register_part_2)
            append(part1)
            append(" ")
            append(part2)

            setSpan(
                ClickableSpan(
                    context.getColor(R.color.text_link), onClickAction
                ),
                part1.length + 1,
                length,
                Spanned.SPAN_EXCLUSIVE_EXCLUSIVE
            )
        }
    }
}
