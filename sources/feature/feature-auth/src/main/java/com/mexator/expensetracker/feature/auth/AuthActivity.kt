package com.mexator.expensetracker.feature.auth

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import com.mexator.expensetracker.feature.auth.databinding.ActivityAuthBinding
import com.mexator.expensetracker.feature.auth.welcome.ui.WelcomeFragment

class AuthActivity: AppCompatActivity(R.layout.activity_auth) {
    private lateinit var binding: ActivityAuthBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityAuthBinding.inflate(layoutInflater)
        setContentView(binding.root)

        supportFragmentManager.beginTransaction()
            .add(R.id.content, WelcomeFragment())
            .commit()
    }
}
