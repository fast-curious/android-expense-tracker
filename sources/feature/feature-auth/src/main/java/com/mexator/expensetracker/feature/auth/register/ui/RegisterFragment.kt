package com.mexator.expensetracker.feature.auth.register.ui

import android.os.Bundle
import android.text.method.LinkMovementMethod
import android.view.View
import androidx.fragment.app.Fragment
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.Transformations.map
import com.google.android.material.snackbar.Snackbar
import com.mexator.expensetracker.common.feature.ui.LiveDataTextWatcher
import com.mexator.expensetracker.common.feature.zip
import com.mexator.expensetracker.feature.auth.R
import com.mexator.expensetracker.feature.auth.StringProvider
import com.mexator.expensetracker.feature.auth.databinding.CredentialsFieldsBinding
import com.mexator.expensetracker.feature.auth.databinding.FragmentRegisterBinding
import com.mexator.expensetracker.feature.auth.register.model.CredentialField.Confirmation
import com.mexator.expensetracker.feature.auth.register.model.CredentialField.Login
import com.mexator.expensetracker.feature.auth.register.model.CredentialField.Password
import com.mexator.expensetracker.feature.auth.register.model.ValidationResult.Ok
import com.mexator.expensetracker.feature.auth.register.model.ValidationResult.Wrong
import com.mexator.expensetracker.feature.auth.register.navigation.RegisterNavigator
import com.mexator.expensetracker.feature.auth.register.presentation.RegisterNews
import com.mexator.expensetracker.feature.auth.register.presentation.RegisterState
import com.mexator.expensetracker.feature.auth.register.presentation.RegisterViewModel
import org.koin.android.ext.android.inject
import org.koin.androidx.viewmodel.ext.android.viewModel

class RegisterFragment : Fragment(R.layout.fragment_register) {
    private lateinit var binding: FragmentRegisterBinding
    private lateinit var credentialsBinding: CredentialsFieldsBinding
    private val viewModel: RegisterViewModel by viewModel()
    private val navigator: RegisterNavigator by inject()
    private val stringProvider: StringProvider by lazy {
        StringProvider(requireContext())
    }

    private val loading = MutableLiveData<Boolean>()

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        binding = FragmentRegisterBinding.bind(view)
        credentialsBinding = CredentialsFieldsBinding.bind(view)

        val loginText = LiveDataTextWatcher()
        val passwordText = LiveDataTextWatcher()
        val confirmText = LiveDataTextWatcher()
        credentialsBinding.loginEdittext.addTextChangedListener(loginText)
        credentialsBinding.passwordEdittext.addTextChangedListener(passwordText)
        binding.passwordConfirmEdittext.addTextChangedListener(confirmText)

        binding.buttonSubmit.setOnClickListener {
            val login = credentialsBinding.loginEdittext.text.toString()
            val password = credentialsBinding.passwordEdittext.text.toString()
            val confirmation = binding.passwordConfirmEdittext.text.toString()
            viewModel.register(login, password, confirmation)
        }

        binding.loginOffer.text =
            stringProvider.getLoginLabelForRegister { navigator.navigateToLogin(requireContext()) }
        binding.loginOffer.movementMethod = LinkMovementMethod.getInstance()

        val fieldsNotEmpty = zip(
            map(loginText, String::isNotBlank),
            map(passwordText, String::isNotBlank),
            map(confirmText, String::isNotBlank)
        ) { a, b, c -> a && b && c }

        zip(fieldsNotEmpty, loading).observe(viewLifecycleOwner) { (fieldsFilled, loading) ->
            binding.buttonSubmit.isEnabled = fieldsFilled && !loading
        }

        viewModel.getState().observe(viewLifecycleOwner, ::render)
        viewModel.getNews().observe(viewLifecycleOwner, ::handleNews)
    }

    private fun render(registerState: RegisterState) {
        loading.value = registerState.isLoading
        when (val result = registerState.validationResult) {
            is Ok -> listOf(
                credentialsBinding.loginInput,
                credentialsBinding.passwordInput,
                binding.passwordConfirmInput
            ).forEach {
                it.error = null
            }

            is Wrong -> when (result.what) {
                Login -> credentialsBinding.loginInput.error = "Bad login"
                Password -> credentialsBinding.passwordInput.error = "Bad password"
                Confirmation -> {
                    listOf(
                        credentialsBinding.passwordInput,
                        binding.passwordConfirmInput
                    ).forEach { it.error = "Password and confirmation not the same!" }
                }
            }
        }
    }

    private fun handleNews(news: RegisterNews) {
        when (news) {
            RegisterNews.SuccessfullyRegistered -> {
                navigator.navigateToTransactions(requireContext())
            }
            RegisterNews.LoginError -> {
                Snackbar
                    .make(binding.root, R.string.error_login, Snackbar.LENGTH_SHORT)
                    .show()
            }
        }
    }
}
