package com.mexator.expensetracker.feature.auth.register.presentation

import com.mexator.expensetracker.feature.auth.register.model.ValidationResult

data class RegisterState(
    val isLoading: Boolean,
    val validationResult: ValidationResult
)
