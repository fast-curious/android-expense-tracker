package com.mexator.expensetracker.feature.auth.welcome.navigation

import android.content.Context

interface WelcomeNavigator {
    fun navigateToLogin(context: Context)
    fun navigateToSignIn(context: Context)
}
