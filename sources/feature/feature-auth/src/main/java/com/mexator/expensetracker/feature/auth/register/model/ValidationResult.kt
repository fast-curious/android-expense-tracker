package com.mexator.expensetracker.feature.auth.register.model

sealed class ValidationResult {
    object Ok : ValidationResult()
    class Wrong(val what: CredentialField) : ValidationResult()
}
