package com.mexator.expensetracker.feature.auth.login.presentation

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.mexator.expensetracker.common.feature.mapValue
import com.mexator.expensetracker.domain.auth.AuthService
import com.mexator.expensetracker.domain.auth.Credentials
import com.mexator.expensetracker.domain.auth.LoginFailed
import com.mexator.expensetracker.feature.auth.login.presentation.LoginNews.LoginError
import com.mexator.expensetracker.feature.auth.login.presentation.LoginNews.SuccessfullyLoggedIn
import kotlinx.coroutines.launch

internal class LoginViewModel(private val authService: AuthService) : ViewModel() {
    private val state = MutableLiveData<LoginState>()
        .also { it.value = LoginState(isLoading = false) }
    private val news = MutableLiveData<LoginNews>()

    fun getState(): LiveData<LoginState> = state
    fun getNews(): LiveData<LoginNews> = news

    fun login(username: String, password: String) {
        viewModelScope.launch {
            try {
                state.mapValue { copy(isLoading = true) }
                authService.login(Credentials(username, password))
                news.value = SuccessfullyLoggedIn
            } catch (_: LoginFailed) {
                news.value = LoginError
            } finally {
                state.mapValue { copy(isLoading = false) }
            }
        }
    }
}
