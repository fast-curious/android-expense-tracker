package com.mexator.expensetracker.feature.auth.login.navigation

import android.content.Context

interface LoginNavigator {
    fun navigateToTransactions(context: Context)
    fun navigateToRegister(context: Context)
}
