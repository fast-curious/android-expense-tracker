package com.mexator.expensetracker.feature.auth.login.ui

import android.os.Bundle
import android.text.method.LinkMovementMethod
import android.view.View
import androidx.fragment.app.Fragment
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.Transformations.map
import com.google.android.material.snackbar.Snackbar
import com.mexator.expensetracker.common.feature.ui.LiveDataTextWatcher
import com.mexator.expensetracker.common.feature.zip
import com.mexator.expensetracker.feature.auth.R
import com.mexator.expensetracker.feature.auth.StringProvider
import com.mexator.expensetracker.feature.auth.databinding.CredentialsFieldsBinding
import com.mexator.expensetracker.feature.auth.databinding.FragmentLoginBinding
import com.mexator.expensetracker.feature.auth.login.navigation.LoginNavigator
import com.mexator.expensetracker.feature.auth.login.presentation.LoginNews
import com.mexator.expensetracker.feature.auth.login.presentation.LoginState
import com.mexator.expensetracker.feature.auth.login.presentation.LoginViewModel
import org.koin.android.ext.android.inject
import org.koin.androidx.viewmodel.ext.android.viewModel

class LoginFragment : Fragment(R.layout.fragment_login) {
    private lateinit var binding: FragmentLoginBinding
    private lateinit var credentialsBinding: CredentialsFieldsBinding
    private val viewModel: LoginViewModel by viewModel()
    private val loginNavigator: LoginNavigator by inject()
    private val stringProvider: StringProvider by lazy {
        StringProvider(requireContext())
    }

    private val loading = MutableLiveData<Boolean>()

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        binding = FragmentLoginBinding.bind(view)
        credentialsBinding = CredentialsFieldsBinding.bind(view)

        val loginText = LiveDataTextWatcher()
        val passwordText = LiveDataTextWatcher()
        credentialsBinding.loginEdittext.addTextChangedListener(loginText)
        credentialsBinding.passwordEdittext.addTextChangedListener(passwordText)

        binding.registerOffer.text =
            stringProvider.getRegisterLabelForLogin {
                loginNavigator.navigateToRegister(
                    requireContext()
                )
            }
        binding.registerOffer.movementMethod = LinkMovementMethod.getInstance()

        val fieldsNotEmpty = map(
            zip(
                map(loginText, String::isNotBlank),
                map(passwordText, String::isNotBlank)
            )
        ) { (a, b) -> a && b }

        zip(fieldsNotEmpty, loading)
            .observe(viewLifecycleOwner) { (fieldsNotEmpty, loading) ->
                binding.buttonSubmit.isEnabled = fieldsNotEmpty && !loading
            }

        binding.buttonSubmit.setOnClickListener {
            val login = credentialsBinding.loginEdittext.text.toString()
            val password = credentialsBinding.passwordEdittext.text.toString()
            viewModel.login(login, password)
        }

        viewModel.getNews().observe(viewLifecycleOwner, ::handleNews)
        viewModel.getState().observe(viewLifecycleOwner, ::render)
    }

    private fun render(state: LoginState) {
        loading.value = state.isLoading
    }

    private fun handleNews(news: LoginNews) {
        when (news) {
            LoginNews.SuccessfullyLoggedIn -> {
                loginNavigator.navigateToTransactions(requireContext())
            }
            LoginNews.LoginError -> {
                Snackbar
                    .make(binding.root, R.string.error_login, Snackbar.LENGTH_SHORT)
                    .show()
            }
        }
    }
}
