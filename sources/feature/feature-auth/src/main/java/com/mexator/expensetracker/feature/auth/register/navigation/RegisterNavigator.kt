package com.mexator.expensetracker.feature.auth.register.navigation

import android.content.Context

interface RegisterNavigator {
    fun navigateToTransactions(context: Context)
    fun navigateToLogin(context: Context)
}
