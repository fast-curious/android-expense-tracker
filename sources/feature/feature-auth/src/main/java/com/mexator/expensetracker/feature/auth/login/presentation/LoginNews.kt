package com.mexator.expensetracker.feature.auth.login.presentation

internal sealed class LoginNews {
    object SuccessfullyLoggedIn: LoginNews()
    object LoginError: LoginNews()
}
