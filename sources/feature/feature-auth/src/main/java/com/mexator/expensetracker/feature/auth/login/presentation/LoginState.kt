package com.mexator.expensetracker.feature.auth.login.presentation

internal data class LoginState(
    val isLoading: Boolean
)
