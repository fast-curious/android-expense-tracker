package com.mexator.expensetracker.feature.auth.di

import android.content.Context
import androidx.appcompat.app.AppCompatActivity
import androidx.fragment.app.commit
import androidx.fragment.app.replace
import com.mexator.expensetracker.feature.auth.R
import com.mexator.expensetracker.feature.auth.login.presentation.LoginViewModel
import com.mexator.expensetracker.feature.auth.login.ui.LoginFragment
import com.mexator.expensetracker.feature.auth.register.presentation.RegisterViewModel
import com.mexator.expensetracker.feature.auth.register.ui.RegisterFragment
import com.mexator.expensetracker.feature.auth.welcome.navigation.WelcomeNavigator
import org.koin.androidx.viewmodel.dsl.viewModel
import org.koin.dsl.module

val featureAuthModule = module {
    viewModel { LoginViewModel(get()) }
    viewModel { RegisterViewModel(get()) }

    single<WelcomeNavigator> {
        object : WelcomeNavigator {
            override fun navigateToLogin(context: Context) {
                (context as AppCompatActivity).supportFragmentManager.commit {
                    replace<LoginFragment>(R.id.content)
                    addToBackStack(null)
                }
            }

            override fun navigateToSignIn(context: Context) {
                (context as AppCompatActivity).supportFragmentManager.commit {
                    replace<RegisterFragment>(R.id.content)
                    addToBackStack(null)
                }
            }
        }
    }
}
