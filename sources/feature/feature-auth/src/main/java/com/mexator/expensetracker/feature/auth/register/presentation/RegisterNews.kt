package com.mexator.expensetracker.feature.auth.register.presentation

internal sealed class RegisterNews {
    object SuccessfullyRegistered : RegisterNews()
    object LoginError : RegisterNews()
}
