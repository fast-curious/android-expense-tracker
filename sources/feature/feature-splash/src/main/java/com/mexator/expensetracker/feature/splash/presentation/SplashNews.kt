package com.mexator.expensetracker.feature.splash.presentation

sealed class SplashNews {
    object AuthValid: SplashNews()
    object NeedLogin: SplashNews()
}
