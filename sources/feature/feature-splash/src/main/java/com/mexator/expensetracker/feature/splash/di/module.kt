package com.mexator.expensetracker.feature.splash.di

import com.mexator.expensetracker.feature.splash.presentation.SplashViewModel
import org.koin.dsl.module

val featureSplashModule = module {
    single {
        SplashViewModel(get())
    }
}
