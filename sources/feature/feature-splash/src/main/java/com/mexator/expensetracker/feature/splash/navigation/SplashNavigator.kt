package com.mexator.expensetracker.feature.splash.navigation

import android.content.Context

interface SplashNavigator {
    fun navigateToLogin(context: Context)
    fun navigateToTransactions(context: Context)
}
