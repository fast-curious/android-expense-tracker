package com.mexator.expensetracker.feature.splash.ui

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import com.mexator.expensetracker.feature.splash.R
import com.mexator.expensetracker.feature.splash.databinding.ActivitySplashBinding
import com.mexator.expensetracker.feature.splash.navigation.SplashNavigator
import com.mexator.expensetracker.feature.splash.presentation.SplashNews
import com.mexator.expensetracker.feature.splash.presentation.SplashViewModel
import org.koin.android.ext.android.inject
import org.koin.androidx.viewmodel.ext.android.viewModel

class SplashActivity : AppCompatActivity(R.layout.activity_splash) {
    private lateinit var binding: ActivitySplashBinding
    private val navigator: SplashNavigator by inject()
    private val viewModel: SplashViewModel by viewModel()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivitySplashBinding.inflate(layoutInflater)
        setContentView(binding.root)

        viewModel.getNews().observe(this) {
            when (it) {
                is SplashNews.AuthValid -> navigator.navigateToTransactions(this)
                is SplashNews.NeedLogin -> navigator.navigateToLogin(this)
            }
        }

        viewModel.init()
    }
}
