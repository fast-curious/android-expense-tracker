package com.mexator.expensetracker.feature.splash.presentation

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.mexator.expensetracker.domain.auth.AuthService
import com.mexator.expensetracker.feature.splash.presentation.SplashNews.AuthValid
import com.mexator.expensetracker.feature.splash.presentation.SplashNews.NeedLogin
import kotlinx.coroutines.launch

internal class SplashViewModel(private val authProvider: AuthService) : ViewModel() {
    private val news: MutableLiveData<SplashNews> = MutableLiveData()
    fun getNews(): LiveData<SplashNews> = news

    fun init() {
        viewModelScope.launch {
            news.value = if (authProvider.needLogin()) NeedLogin else AuthValid
        }
    }
}
